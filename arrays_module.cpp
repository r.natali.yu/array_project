#include <iostream>
#include <time.h>

int main()
{
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);

    const int N = 7;
    int array[N][N];
    int sum = 0;

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
            std::cout << array[i][j] << ' ';

            if (i == buf.tm_mday % N)
            {
                sum += array[i][j];
            }
        }
        std::cout << std::endl;

    }

    std::cout << sum << std::endl;
    //std::cout << buf.tm_mday << std::endl << buf.tm_mday % N << std::endl;
}
